<?php

namespace App\Http\Controllers\Api;

use App\Api\ApiMessages;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReportRequest;
use App\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private $report;

    public function __construct(Report $report){
        $this->report = $report;
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $report = auth('api')->user()->reports;
        #$report = $this->report->all();
        return response()->json($report, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReportRequest $request)
    {
        $data = $request->all();
        try{
            $data['user_id'] = auth('api')->user()->id;
            $report = $this->report->create($data);            
            return response()->json([
                'data'=> [
                    'msg'=> 'Relatório cadastrado com sucesso!'
                ]
            ], 200);
        } catch(\Exception $e){
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 401);
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $report = auth('api')->user()->reports()->findOrFail($id);
            return response()->json(['data' => $report], 200);
        } catch (\Exception $e){
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReportRequest $request, $id)
    {
        $data = $request->all();
        $data['user_id'] = auth('api')->user()->id;
        try {
            $report = auth('api')->user()->reports()->findOrFail($id);
            $report->update($data);
            return response()->json([
                'data'=> [
                    'msg'=> 'Relatório alterado com sucesso!'
                ]
            ], 200);            
        } catch(\Exception $e){
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 401);        
        }     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{    
            $report = auth('api')->user()->reports()->findOrFail($id);
            $report->delete();
            return response()->json([
                'data'=> [
                    'msg'=> 'Relatório removido com sucesso!'
                ]
            ], 200);
        } catch(\Exception $e){
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 401); 
        }
    }
}
