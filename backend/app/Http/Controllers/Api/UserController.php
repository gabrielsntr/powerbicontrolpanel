<?php

namespace App\Http\Controllers\Api;

use App\Api\ApiMessages;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(User $user){
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $users = $this->user->paginate('10');

        return response()->json($users, 200);
    }

    public function show($id){
        try{
            $user = $this->user->with('reports')->findOrFail($id);
            return response()->json(['data' => $user], 200);
        } catch (\Exception $e){
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 401);
        }
        
    }

    public function store(UserRequest $request){        
        
        $data = $request->all();
        if (!$request->has('password') || !$request->get('password')){
            $message = new ApiMessages("Please inform the user password!");
            return response()->json($message->getMessage());
        }

        try{
            $data['password'] = bcrypt($data['password']);
            $user = $this->user->create($data);

            return response()->json([
                'data'=> [
                    'msg'=> 'Usuário cadastrado com sucesso!'
                ]
            ], 200);
        } catch(\Exception $e){
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 401);
        }        
    }

    public function update($id, Request $request){        
        $data = $request->all();
        if (!$request->has('password') || !$request->get('password')){
            $message = new ApiMessages("Please inform the user password!");
            return response()->json($message->getMessage());
        }

        try{
            $data['password'] = bcrypt($data['password']);
            $user = $this->user->findOrFail($id);
            $user->update($data);
            return response()->json([
                'data'=> [
                    'msg'=> 'Usuário alterado com sucesso!'
                ]
            ], 200);
        } catch(\Exception $e){
            $message = new ApiMessages($e->getMessage());            
            return response()->json($message->getMessage(), 401);        
        }
    }

    public function destroy($id){
        
        try{    
            $user = $this->user->findOrFail($id);
            $user->delete();
            return response()->json([
                'data'=> [
                    'msg'=> 'Usuário removido com sucesso!'
                ]
            ], 200);
        } catch(\Exception $e){
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 401);        
        }        
    }
}
