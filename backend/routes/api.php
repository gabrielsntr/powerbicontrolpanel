<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::prefix('v1')->namespace('Api')->group(function(){
    
    Route::post('/login', 'Auth\\LoginJwtController@login')->name('login');
    Route::get('/logout', 'Auth\\LoginJwtController@logout')->name('logout');
    Route::get('/refresh', 'Auth\\LoginJwtController@refresh')->name('refresh');


   Route::group(['middleware' => ['jwt.auth']], function(){
    Route::name('users')->group(function(){
        Route::resource('users', 'UserController');
    });

    Route::name('reports')->group(function(){
        Route::resource('reports', 'ReportController');
    });           
   });
});