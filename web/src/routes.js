import React from 'react';
import SignIn from './pages/SignIn';
import Main from './pages/Main';
import { BrowserRouter, Router, Switch, Redirect, Route } from "react-router-dom";
import { isAuthenticated } from "./services/auth";

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
    {...rest}
    render={props => isAuthenticated() ? (
        <Component {...props} />
    ) : (
        <Redirect to={{pathname: "/", state: { from: props.location } }} />
    )
    }
    />
);


const Routes = () => (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={SignIn} />
        <PrivateRoute path="/app" component={Main} />
        <Route path="*" component={() => <h1>Page not found</h1>} />
      </Switch>
    </BrowserRouter>
  );
  
  export default Routes;