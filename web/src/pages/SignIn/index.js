import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import Logo from "../../assets/logo.png";
import api from "../../services/api";
import { login } from "../../services/auth";
import "./styles.css";

class SignIn extends Component {
  state = {
    email: "",
    password: "",
    error: ""
  };

  handleSignIn = async e => {
    e.preventDefault();
    const { email, password } = this.state;
    if (!email || !password) {
      this.setState({ error: "Preencha e-mail e senha para continuar!" });
    } else {
      try {
        const response = await api.post("login", { email, password });
        login(response.data.token);
        this.props.history.push("/app");
      } catch (err) {
        this.setState({
          error:
            "Houve um problema com o login, verifique suas credenciais."
        });
      }
    }
  };

  

  render() {
    return (
     <div className="inner-container">
        <form className="box" onSubmit={this.handleSignIn}>        
          <img src={Logo} alt="Random logo" />          
          <div className="input-group">
            <input
              type="text"
              name="username"
              className="login-input"
              onChange={e => this.setState({ email: e.target.value })}
              placeholder="E-mail"/>
            </div>
            <div className="input-group">
              <input
                type="password"
                name="password"
                onChange={e => this.setState({ password: e.target.value })}              
                className="login-input"
                placeholder="Senha"/>
            </div>          
          <button
            type="submit"            
            className="login-btn" >Login
          </button>
            {this.state.error && <p className="errorMsg">{this.state.error}</p>}
        </form>              
      </div> 
    );
  }
}

export default withRouter(SignIn);