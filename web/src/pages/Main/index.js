import React from 'react';
import { Link, withRouter } from "react-router-dom";
import Sidebar from '../../components/sidebar/sidebar'
import './styles.css'



function Main(){
    return (
        <div id="root">
            <Sidebar/>
        </div>
    )
}



export default withRouter(Main);