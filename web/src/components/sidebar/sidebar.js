import React, {useState, useEffect} from 'react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import api from '../../services/api'
import hamburger from "../../assets/open-menu.svg";
import './styles.css'


let isMenu = true;
function controlMenu(){
  console.log(isMenu);
  if (isMenu) {
    document.getElementById('sidebar').style.marginLeft = '0';
    document.getElementById('frame').style.marginLeft = '250';
  } else {
    document.getElementById('sidebar').style.marginLeft = '-250';
    document.getElementById('frame').style.marginLeft = '0';
  }
}



function Sidebar() {
    
    const [reports, setReports] = useState([]);
    let frameUrl = '';

    

    useEffect(() => {
        async function loadReports() {
            const response = await api.get('reports');
            setReports(response.data);
        }
        loadReports();
    }, []);

      return (
        <div className="main">
          <div id="sidebar" className="sidebar">
            <p>Relatórios</p>
            <List disablePadding dense>
              {reports.map(item => (
                <ListItem key={item.id} button>
                <ListItemText>{item.name}</ListItemText>
                </ListItem>
              ))}
            </List>        
          </div>
          <img src={hamburger} alt="Menu" className="menu-btn" onClick={controlMenu}>
          </img>
          <div className="frame">
            <iframe src={frameUrl}></iframe>
          </div>
        </div>
  )
}



export default Sidebar